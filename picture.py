# coding=utf-8
#***************CAPTURE PICTURE FROM CAMERA**************************
#
import json
import time
from camera import Camera
import picamera
from log import Log

class Picture:
    def __init__(self):
        log = Log("Picture")
        self.logger = log.logger
        settings = json.load(open("settings.json"))["Picture"]

    def UpdatePictureSettings(self):
        self.logger.info("Update picture settings")
        settings = json.load(open("settings.json"))["Picture"]
        self.Counter =  settings["Counter"]
        self.Interval = settings["Interval"]

    def SnapShot(self):  
        try:
            cameraSettings = Camera()
            camera = picamera.PiCamera()
            camera.vflip = cameraSettings.Vflip
            camera.hflip = cameraSettings.Hflip       
            datetime = time.strftime("%Y-%m-%d %H:%M:%S")
            camera.capture('pics/'+datetime+'.png')
            self.logger.info("Picture #%d taken", cameraSettings.GetCounter())

        except:
            self.logger.exception("Picture failed")