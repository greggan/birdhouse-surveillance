# coding=utf-8
import json
from log import Log
import os

class Camera:
    def __init__(self):
        log = Log("Camera")
        self.logger = log.logger

        settings = json.load(open("settings.json"))["Camera"]
        self.Sharpness = settings["Sharpness"]
        self.Contrast = settings["Contrast"]
        self.Brightness = settings["Brightness"]
        self.Saturation = settings["Saturation"]
        self.ISO = settings["ISO"]
        self.Video_stabilization = settings["Video_stabilization"]
        self.Exposure_compensation = settings["Exposure_compensation"]
        self.Exposure_mode = settings["Exposure_mode"]
        self.Meter_mode = settings["Meter_mode"]
        self.Image_effect = settings["Image_effect"]
        self.Color_effects = settings["Color_effects"]
        self.Rotation = settings["Rotation"]
        self.Hflip = settings["Hflip"]
        self.Vflip = settings["Vflip"]
        self.Crop = settings["Crop"]

    def GetCounter(self):
        try:
            counter = 1
            if os.path.exists('logs/counter.log'):
                counterFile = open("logs/counter.log")
                line = counterFile.readline()
                #Om det finns en fil som inte är tom.
                if (line.strip() != ''): 
                    counter = int(line) + 1  
            else:
                self.logger.info("Creating log file: counter.log")
                counterFile = open("logs/counter.log", 'w', encoding = "utf8")

            counterFile.close()
            self.UpdateCounter(counter)
            return counter

        except Exception:
            self.logger.exception("Cannot read the counter")


    def UpdateCounter(self, updatedCounter):
        counterFile = open("logs/counter.log", "w", encoding = "utf8")
        counterFile.write(str(updatedCounter))
        counterFile.close()
