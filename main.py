
# coding=utf-8
from picture import Picture
from communication import Communication
from log import Log
import RPi.GPIO as GPIO
from time import sleep
import threading

def main():
    picture = Picture()
    log = Log("main")
    logger = log.logger
    logger.info("Program started")

    communicationThread = threading.Thread(target=Communication)
    communicationThread.start()

    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(11, GPIO.IN) 
        
    while True:        
        motionDetected=GPIO.input(11)

        if(motionDetected == True):
            logger.info("MOTION DETECTED, WE HAVE A BIRD?")
            picture.SnapShot()

        else:
            print("No bird :(:(")
        
        # sleep innan den kollar rörelsesensorn igen
        sleep(1)



if __name__ == '__main__':
    main()