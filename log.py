import logging

class Log:
    def __init__(self, name):
        # Formaterar error meddelande enligt "tid [filnamn] lognivå(info, error etc) - meddelande"
        formatter = logging.Formatter("%(asctime)s [%(name)s] %(levelname)s - %(message)s")

        # Filhanteraren där det specifieras var vi ska logga
        fileHandler = logging.FileHandler('logs/birdhouse.log', 'a+')
        fileHandler.setFormatter(formatter)

        # Skriver ut log-meddelandena i consolen
        streamHandler= logging.StreamHandler()
        streamHandler.setFormatter(formatter)

        # logger är loggingobjectet som används för att logga
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(fileHandler)
        self.logger.addHandler(streamHandler)

 