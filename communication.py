# coding=utf-8
#***************DATA COMMUNICATION***********************************
#

from ftplib import FTP
from time import sleep
import os
from log import Log


class Communication:
    def __init__(self):
        log = Log("Communication")
        self.logger = log.logger
        self.logger.info("Communication script started")

        self.ftp = FTP('orvar.i234.me')  # connect to host, default port
        self.ftp.login('birdhouse', 'vagelholk')
        self.ftp.cwd('BIRDHOUSE/pics')
        #self.ftp.retrlines('LIST')

        while True:
            self.CheckNewFiles()
            sleep(30)  # kollar efter nya filer var 30:e sekund

# OBSS! det blir timeout till nasen efter 300 sekunder så vi måste nog 
# connecta och reconnecta till Nasen innan och efter denna funktion
    def CheckNewFiles(self):
        allFileNames = os.listdir('pics/')
        allFileNames.remove(".empty")
        if (len(allFileNames) == 0):
            print("No new files")
            return None

        for fileName in allFileNames:
            response = self.SendFile(fileName)
            # om responset innehåller returnkod 226 (OK) -> ta bort filen
            if (response != None and "226" in response):
                self.logger.info("Removing file: " + fileName)
                os.remove('pics/' + fileName)

    def SendFile(self, fileName):
        try:
            file = open('pics/' + fileName, 'rb')
            self.logger.info("Sending file: " + fileName)
            response = self.ftp.storbinary('STOR %s' % fileName, file)
            self.logger.info("Response: " + response)
            file.close()
            return response
        except:
            self.logger.exception("FAN! pga: ")

    def Receive(self):
        pass